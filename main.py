import discord
import os

client = discord.Client()

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('Viva emperor!'):
        await message.channel.send('Viva!')
    if message.content.startswith('Что у нас бывает за нарушение правил?'):
        await message.channel.send('Суд трибунов назначает наказание. Все зависит от нарушения.')
    if message.content.startswith('Нарушение'):
        await message.channel.send('Суд трибунов выносит вам предупреждение.')
    if message.content.startswith('Карфаген'):
        await message.channel.send('Должен быть разрушен.')
    if message.content.startswith('0AD'):
        await message.channel.send('Лучшая RTS!')

client.run('TOKEN')
